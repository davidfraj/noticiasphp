<?php
//Si fuera necesario.... declaro la variable de session
if(!isset($_SESSION['conectado'])){
  //Si hay una cookie, conectamos al usuario
  if(isset($_COOKIE['correoUsu'])){
    $_SESSION['conectado']=true;
  }else{
    $_SESSION['conectado']=null;
  }
  // fin de comprobar si hay cookie
}

if(!isset($_SESSION['usuario'])){
  //Si hay una cookie, conectamos al usuario
  if(isset($_COOKIE['correoUsu'])){
    $correoUsu=$_COOKIE['correoUsu'];
    $sessionUsu=$_COOKIE['sessionUsu'];
    $sql="SELECT * FROM usuarios WHERE correoUsu='$correoUsu' AND sessionUsu='$sessionUsu'";
    $consulta=mysqli_query($conexion,$sql);
    $fila=mysqli_fetch_array($consulta);
    $_SESSION['usuario']=$fila;
  }else{
    $_SESSION['usuario']=null;
  }
  // fin de comprobar si hay cookie
}



//Posibilidad de que el usuario se conecte
if(isset($_POST['entrar'])){
  $email=$_POST['email'];
  $password=md5($_POST['password']);

  $sql="SELECT * FROM usuarios WHERE correoUsu='$email' AND claveUsu='$password'";
  $consulta=mysqli_query($conexion, $sql);

  if($fila=mysqli_fetch_array($consulta)){
    $_SESSION['conectado']=true;
    $_SESSION['usuario']=$fila;

    //Si hemos marcado el checkbox, CREAMOS la COOKIE
    if(isset($_POST['mantener'])){
      setcookie('correoUsu', $email, time()+(60*60*24*7));
      $sessionUsu=session_id();
      setcookie('sessionUsu', $sessionUsu, time()+(60*60*24*7));
      $sql="UPDATE usuarios SET sessionUsu='$sessionUsu' WHERE correoUsu='$email'";
      mysqli_query($conexion, $sql);
    }
  }
}
//echo session_id(); //Devuelve el SESSIONID
//Posibilidad de que el usuario se desconecte
if(isset($_GET['desconectar'])){
  
  //Quitamos el sessionUsu de la BBDD
  if(isset($_COOKIE['sessionUsu'])){
    $sessionUsu=$_COOKIE['sessionUsu'];
    $sql="UPDATE usuarios SET sessionUsu='' WHERE sessionUsu='$sessionUsu'";
    $consulta=mysqli_query($conexion, $sql);
  }

  $_SESSION['conectado']=null;
  $_SESSION['usuario']=null;
  setcookie('correoUsu', '', 0);
  setcookie('sessionUsu', '', 0);

}

//Posibilidad de comprobar si estamos o no conectados
if($_SESSION['conectado']==true){
  ?>
  <h2 class="text-right">
  Bienvenido <?php echo $_SESSION['usuario']['nombreUsu']; ?>
  <br>
  <small>
  <a href="index.php?desconectar=si">Desconectar</a>
  </small>
  </h2>
  <?php
}else{
?>

<br>
<form class="form" role="form" action="index.php" method="post">
  <div class="form-group">
    <label class="sr-only" for="email">Email</label>
    <input type="email" class="form-control" id="email"
           placeholder="Introduce tu email" name="email">
  </div>
  <div class="form-group">
    <label class="sr-only" for="password">Contraseña</label>
    <input type="password" class="form-control" id="password" 
           placeholder="Contraseña" name="password">
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox" name="mantener"> No cerrar sesión
    </label>
    <button type="submit" class="btn btn-default" name="entrar">Entrar</button> - <a href="index.php?p=registro.php">Registrate</a>
  </div>

</form>

<?php
}
?>