<?php  

//$paginas=['productos.php'];
$paginas=['productos.html'];
$enlaces=['Listado de productos'];

if($_SESSION['conectado']){
  if($_SESSION['usuario']['tipoUsu']=='administrador'){
  	//$paginas=['productos.php', 'categorias.php'];
    $paginas=['productos.html', 'categorias.html'];
  	$enlaces=['Listado de productos', 'Gestor de categorias'];
  }else{
    //$paginas=['productos.php', 'pedidos.php'];
    $paginas=['productos.html', 'pedidos.html'];
    $enlaces=['Listado de productos', 'Pedidos anteriores'];
  }
}
?>
<ul class="nav nav-tabs">
  <?php  
  for ($i=0; $i < count($paginas); $i++) { 
  	if($p==$paginas[$i]){
  		$clase='active';
  	}else{
  		$clase='';
  	}
  	?>
  	<li class="<?php echo $clase;?>">
  		<!-- <a href="index.php?p=<?php echo $paginas[$i];?>"> -->
      <a href="<?php echo $paginas[$i];?>">
  			<?php echo $enlaces[$i];?>
  		</a>
  	</li>
  	<?php
  }
  ?>
</ul>