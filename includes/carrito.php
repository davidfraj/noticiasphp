<h3>Carrito de compra</h3>
<hr>
<?php 
if(!isset($_SESSION['carrito'])){
	//Si no tengo esta variable, la creo, diciendo que es un array
	$_SESSION['carrito']=[];
	$_SESSION['uniCarrito']=[];
}

//Que pasa cuando quiero añadir un producto al carrito
if(isset($_GET['idCompra'])){
	if(in_array($_GET['idCompra'], $_SESSION['carrito'])){
		$posicion=array_search($_GET['idCompra'], $_SESSION['carrito']);
		$_SESSION['uniCarrito'][$posicion]++;
	}else{
		$_SESSION['carrito'][]=$_GET['idCompra'];
		$_SESSION['uniCarrito'][]=1;
	}
}

//Si quiero vaciar la cesta
if(isset($_GET['vaciarCesta'])){
	$_SESSION['carrito']=[];
	$_SESSION['uniCarrito']=[];
}

//Si quiero quitar una unidad del carrito
if(isset($_GET['quitar'])){
	$quitar=$_GET['quitar'];
	if($_SESSION['uniCarrito'][$quitar]>1){
		//Quitamos una unidad
		$_SESSION['uniCarrito'][$quitar]--;
	}else{
		//Eliminamos el producto del carrito
		array_splice($_SESSION['uniCarrito'], $quitar, 1);
		array_splice($_SESSION['carrito'], $quitar, 1);
	}
}

if(isset($_GET['quitarTodos'])){
	$quitarTodos=$_GET['quitarTodos'];
	//Eliminamos el producto del carrito
	array_splice($_SESSION['uniCarrito'], $quitarTodos, 1);
	array_splice($_SESSION['carrito'], $quitarTodos, 1);
}

//Mostramos el contenido del carrito
$total=0;
foreach ($_SESSION['carrito'] as $key => $value) {
	$sql="SELECT * FROM productos WHERE idPro=$value";
	$consulta=mysqli_query($conexion, $sql);
	$fila=mysqli_fetch_array($consulta);

	$subtotal=$_SESSION['uniCarrito'][$key]*$fila['precioPro'];
	$total+=$subtotal;

	echo $fila['nombrePro'];
	echo ' - ';
	echo $_SESSION['uniCarrito'][$key].' u';
	echo ' - ';
	echo $fila['precioPro'].' €/u';
	echo ' - ';
	echo $subtotal.' €';
	echo ' ';
	echo '<a href="index.php?p=productos.php&quitar='.$key.'"> - </a>';
	echo ' ';
	echo '<a href="index.php?p=productos.php&idCompra='.$_SESSION['carrito'][$key].'"> + </a>';
	echo ' ';
	echo '<a href="index.php?p=productos.php&quitarTodos='.$key.'"> X </a>';
	echo '<br>';
}
echo '<hr>';
echo 'Total: '.$total.' €<br>';

//Muestro el numero de productos en la cesta
echo count($_SESSION['carrito']),' productos en cesta.'
?>
- <a href="index.php?p=productos.php&vaciarCesta=si">Vaciar</a>
<!-- - <a href="index.php?p=cesta.php">Ver Cesta</a> -->
- <a href="cesta.html">Ver Cesta</a>