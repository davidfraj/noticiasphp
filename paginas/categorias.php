<?php  
//Recojo la accion a realizar
$accion='listado';

if($_SESSION['conectado']){
  if($_SESSION['usuario']['tipoUsu']=='administrador'){
		if(isset($_GET['accion'])){
			$accion=$_GET['accion'];
		}else{
			$accion='listado';
		}
  }
}else{
	header('Location:index.php');
}


//Elijo entre la accion que quiere realizar el usuario
switch($accion){
	case 'borrar':
		/***
		 *    888b. .d88b. 888b. 888b.    db    888b. 
		 *    8wwwP 8P  Y8 8  .8 8  .8   dPYb   8  .8 
		 *    8   b 8b  d8 8wwK' 8wwK'  dPwwYb  8wwK' 
		 *    888P' `Y88P' 8  Yb 8  Yb dP    Yb 8  Yb 
		 *                                            
		 */
		?>
		<h2>
			Borrar categoria - 
			<small>
				Borramos la categoria
			</small>
		</h2>
		<br>
		<?php 
		//Recogemos el id de categoria que queremos borrar
		$id=$_GET['id'];

		//Pensamos la pregunta a la base de datos
		$sql="DELETE FROM categorias WHERE idCat=$id";

		//Realizamos la pregunta
		$consulta=mysqli_query($conexion, $sql);

		if($consulta==true){
			echo 'Consulta realizada con exito';
			header('Location:index.php?p=categorias.php');
		}else{
			echo $sql;
			echo '<br><hr>Error de consulta';
		}

		?>
		<?php
		break;
	case 'insertar':
		/***
		 *    888 8b  8 .d88b. 8888 888b. 88888    db    888b. 
		 *     8  8Ybm8 YPwww. 8www 8  .8   8     dPYb   8  .8 
		 *     8  8  "8     d8 8    8wwK'   8    dPwwYb  8wwK' 
		 *    888 8   8 `Y88P' 8888 8  Yb   8   dP    Yb 8  Yb 
		 *                                                     
		 */
		?>
		<h2>
			Alta de categoria - 
			<small>
				Insertar una nueva categoria
			</small>
			-
			<small>
				<a href="index.php">Cancelar</a>
			</small>
		</h2>
		<br>
		<form action="index.php?p=categorias.php&accion=insercion" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">

				<label for="nombreCat">Nombre:</label>
				<input type="text" name="nombreCat" id="nombreCat" class="form-control">

				<br><hr>
				<input type="submit" value="Alta categoria" name="insertar" class="btn btn-default">
			</div>
		</form>	
		<?php
		break;
	case 'insercion':
		/***
		 *    888 8b  8 .d88b. 8888 888b. .d88b 888 .d88b. 8b  8 
		 *     8  8Ybm8 YPwww. 8www 8  .8 8P     8  8P  Y8 8Ybm8 
		 *     8  8  "8     d8 8    8wwK' 8b     8  8b  d8 8  "8 
		 *    888 8   8 `Y88P' 8888 8  Yb `Y88P 888 `Y88P' 8   8 
		 *                                                       
		 */
		?>
		<h2>
			Alta de categorias - 
			<small>
				Insercion de categorias
			</small>
		</h2>
		<br>
		<?php  
		//Recojo los datos que quiero insertar
		$nombreCat=$_POST['nombreCat'];
		
		//Pensamos la pregunta a SQL
		$sql="INSERT INTO categorias(nombreCat)VALUES('$nombreCat')";

		//Realizamos la pregunta
		$consulta=mysqli_query($conexion, $sql);

		if($consulta==true){
			echo 'Consulta realizada con exito';
			header('Location:index.php?p=categorias.php');
		}else{
			echo $sql;
			echo '<br><hr>Error de consulta';
		}
		?>
		<?php
		break;
	case 'modificar':
		/***
		 *    8b   d8 .d88b. 888b. 888 8888 888 .d88b    db    888b. 
		 *    8YbmdP8 8P  Y8 8   8  8  8www  8  8P      dPYb   8  .8 
		 *    8  "  8 8b  d8 8   8  8  8     8  8b     dPwwYb  8wwK' 
		 *    8     8 `Y88P' 888P' 888 8    888 `Y88P dP    Yb 8  Yb 
		 *                                                           
		 */
		?>
		<?php  
		//Recojo el id del producto que quiero modificar
		$id=$_GET['id'];

		//Pienso la pregunta
		$sql="SELECT * FROM categorias WHERE idCat=$id";

		//Ejecuto la consulta
		$consulta=mysqli_query($conexion, $sql);

		//Extraigo ese unico producto
		$fila=mysqli_fetch_array($consulta);
		?>
		<h2>
			Modificar categorias - 
			<small>
				Modificar una categoria
			</small>
			-
			<small>
				<a href="index.php">Cancelar</a>
			</small>
		</h2>
		<br>
		<form action="index.php?p=categorias.php&accion=modificacion" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">

				<label for="nombreCat">Nombre:</label>
				<input type="text" name="nombreCat" id="nombreCat" class="form-control" value="<?php echo $fila['nombreCat'];?>">

				<input type="hidden" name="idCat" value="<?php echo $fila['idCat'];?>">

				<input type="submit" value="Guardar categoria" name="modificar" class="btn btn-default">
			</div>
		</form>
		<?php
		break;
	case 'modificacion':
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		// MODIFICACION ///////////////////////////////////////
		///////////////////////////////////////////////////////
		?>
		<h2>
			Modificar categorias - 
			<small>
				Modificacion una categoria
			</small>
		</h2>
		<br>
		<?php  
		//Recojo los datos
		$nombreCat=$_POST['nombreCat'];
		$idCat=$_POST['idCat'];

		//Preparamos la pregunta
		$sql="UPDATE categorias SET nombreCat='$nombreCat' WHERE idCat=$idCat";

		//Realizamos la pregunta
		$consulta=mysqli_query($conexion, $sql);

		if($consulta==true){
			echo 'Consulta realizada con exito';
			header('Location:index.php?p=categorias.php');
		}else{
			echo $sql;
			echo '<br><hr>Error de consulta';
		}
		?>
		<?php
		break;
	case 'listado':
	default:
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		// LISTADO DE ELEMENTOS ///////////////////////////////
		///////////////////////////////////////////////////////
		?>
		<h2>
			Listado de categorias - 
			<small>
				<a href="index.php?p=categorias.php&accion=insertar">
					Dar de alta una nueva categoria
				</a>
			</small>
		</h2>
		<br>
		<?php  
		//Pensar la pregunta que quiero hacer
		$sql="SELECT count(productos.idCat) AS totalCat, categorias.idCat AS idCat, nombreCat FROM categorias LEFT JOIN productos ON categorias.idCat=productos.idCat GROUP BY categorias.idCat ORDER BY nombreCat";

		//$sql="SELECT * FROM categorias ORDER BY nombreCat";


		//Realizamos la pregunta
		$consulta=mysqli_query($conexion, $sql);

		//Analizamos la respuesta
		while($fila=mysqli_fetch_array($consulta)){

			// $sql2="SELECT * FROM productos WHERE idCat=".$fila['idCat'];
			// $consulta2=mysqli_query($conexion, $sql2);
			// $totalCat=mysqli_num_rows($consulta2);

			?>
			<article>
				<header>
					<h2>
						<?php echo $fila['nombreCat'];?>
						
						<span class="label label-default"><?php echo $fila['totalCat'];?></span>
						
						-
						<small>
							<a href="index.php?p=categorias.php&accion=borrar&id=<?php echo $fila['idCat'];?>" onClick="if(!confirm('Estas seguro?')){return false;};">Borrar</a>
							
							<a href="index.php?p=categorias.php&accion=modificar&id=<?php echo $fila['idCat'];?>">Modificar</a>
						</small>
					</h2>
				</header>
			</article>
			<div style="clear:both;"></div>
			<hr>
			<?php
		}
		?>
		<?php
		break;
}
?>