<h2>
	Pedidos realizados
</h2>
<hr>
<div id="acordeon">
<?php 
$idUsu=$_SESSION['usuario']['idUsu'];
$sql="SELECT pedidos.idPed AS idPed, fechaPed, sum(precioProPed*unidadesProPed) AS totalPed FROM pedidos INNER JOIN productosenpedido ON pedidos.idPed=productosenpedido.idPed WHERE idUsu=$idUsu GROUP BY productosenpedido.idPed ORDER BY fechaPed DESC";

$consulta=mysqli_query($conexion, $sql);

while($fila=mysqli_fetch_array($consulta)){
	?>
	<h3>
		<?php echo $fila['fechaPed'];?>
		 - 
		<?php echo number_format($fila['totalPed'],2);?>
		 Euros	
	</h3>
	<div>
		<table class="table">
			<tr>
				<th>Nombre</th>
				<th>Imagen</th>
				<th>Precio</th>
				<th>Unidades</th>
				<th>Total</th>
			</tr>
			<?php
			$idPed=$fila['idPed'];  
			$sqlP="SELECT precioProPed, unidadesProPed, (precioProPed*unidadesProPed) AS totalProPed, imagenPro, nombrePro FROM productosenpedido INNER JOIN productos ON productosenpedido.idPro=productos.idPro WHERE productosenpedido.idPed=$idPed";
			$consultaP=mysqli_query($conexion, $sqlP);
			while($filaP=mysqli_fetch_array($consultaP)){
				?>
				<tr>
					<td><?php echo $filaP['nombrePro'];?></td>
					<td><img src="imagenes/productos/<?php echo $filaP['imagenPro'];?>" width="50"></td>
					<td><?php echo number_format($filaP['precioProPed'],2);?> Euros</td>
					<td><?php echo $filaP['unidadesProPed'];?> Unidades</td>
					<td><?php echo number_format($filaP['totalProPed'],2);?></td>
				</tr>
				<?php
			}
			?>
			<tr>
			<td colspan="5" align="right">
			<?php echo number_format($fila['totalPed'],2);?> Euros
			</td>
			</tr>
		</table>
	</div>
	<?php
}
?>
</div>