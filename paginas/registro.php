<h2>
	Registro de usuarios - 
	<small>
		Registrate como usuario
	</small>
	-
	<small>
		<a href="index.php">Cancelar</a>
	</small>
</h2>
<br>

<?php  
if(isset($_POST['registrar'])){
	//Partimos que los datos son correctos
	$datosCorrectos=true;
	$mensajeError='';

	$nombreUsu=$_POST['nombreUsu'];
	if(strlen($nombreUsu)<5){
		$datosCorrectos=false;
		$mensajeError.='El nombre de usuario, debe de ser de al menos 5 letras<br>';
	}

	$claveUsu=$_POST['claveUsu'];
	$claveUsu2=$_POST['claveUsu2'];
	if(strlen($claveUsu)<8){
		$datosCorrectos=false;
		$mensajeError.='La clave de usuario debe de ser de al menos 8 caracteres<br>';
	}
	if($claveUsu!=$claveUsu2){
		$datosCorrectos=false;
		$mensajeError.='Las claves no coinciden<br>';
	}

	$correoUsu=$_POST['correoUsu'];
	$sql="SELECT * FROM usuarios WHERE correoUsu='$correoUsu'";
	$consulta=mysqli_query($conexion, $sql);
	//if($fila=mysqli_fetch_array($consulta)){
	if(mysqli_num_rows($consulta)!=0){
		$datosCorrectos=false;
		$mensajeError.='El Correo del usuario YA esta en nuestra base de datos<br>';
	}
	if(strlen($correoUsu)<5){
		$datosCorrectos=false;
		$mensajeError.='La direccion de correo, debe de ser de al menos 5 letras<br>';
	}

	if($datosCorrectos==true){

		$claveUsu=md5($claveUsu);
		$fechaAltaUsu=date('Y-m-d H:i:s');
		$tipoUsu='estandard';

		$sql="INSERT INTO usuarios(nombreUsu, correoUsu, claveUsu, fechaAltaUsu, tipoUsu)VALUES('$nombreUsu', '$correoUsu', '$claveUsu', '$fechaAltaUsu', '$tipoUsu')";
		$consulta=mysqli_query($conexion, $sql);

		if($consulta==true){
			echo 'OK';
			header('Location:index.php');
		}else{
			echo 'ERROR';
		}


	}else{
		?>
		<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Error!</strong> <br>
		  <?php echo $mensajeError; ?>
		</div>
		<?php
	}


}
?>

<form action="index.php?p=registro.php" method="post" class="form-horizontal">
<div class="form-group">

	<label for="nombreUsu">Nombre:</label>
	<input type="text" name="nombreUsu" id="nombreUsu" class="form-control">

	<label for="correoUsu">Correo:</label>
	<input type="text" name="correoUsu" id="correoUsu" class="form-control">

	<label for="claveUsu">Clave:</label>
	<input type="password" name="claveUsu" id="claveUsu" class="form-control">

	<label for="claveUsu2">Clave:</label>
	<input type="password" name="claveUsu2" id="claveUsu2" class="form-control">

	<br><hr>
	<input type="submit" value="Alta de usuario" name="registrar" class="btn btn-default">
</div>
</form>
