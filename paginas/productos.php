<?php  
//Recojo la accion a realizar
//Acciones disponibles para TODOS los usuario
$accionesDisponibles=array('listado', 'ver');
if($_SESSION['conectado']){
	if($_SESSION['usuario']['tipoUsu']=='administrador'){
		$accionesDisponibles=array('listado', 'insertar', 'insercion', 'modificar', 'modificacion', 'borrar', 'ver');
	}
}

$accion='listado';
if(isset($_GET['accion'])){
	if(in_array($_GET['accion'], $accionesDisponibles)){
		$accion=$_GET['accion'];
	}
}



//Elijo entre la accion que quiere realizar el usuario
switch($accion){
	case 'borrar':
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		// BORRAR /////////////////////////////////////////////
		///////////////////////////////////////////////////////
		?>

		
		<h2>
			Borrar producto - 
			<small>
				Borramos el producto
			</small>
		</h2>
		<br>
		<?php 
		//Recogemos el id de producto que queremos borrar
		$id=$_GET['id'];

		//Borramos la imagen FISICAMENTE del directorio
		$sql="SELECT * FROM productos WHERE idPro=$id";
		$consulta=mysqli_query($conexion, $sql);
		$fila=mysqli_fetch_array($consulta);
		if($fila['imagen']!='ninguna.png'){
			unlink('imagenes/productos/'.$fila['imagen']);
		}

		//Pensamos la pregunta a la base de datos
		$sql="DELETE FROM productos WHERE idPro=$id";

		//Realizamos la pregunta
		$consulta=mysqli_query($conexion, $sql);

		if($consulta==true){
			echo 'Consulta realizada con exito';
			header('Location:index.php?p=productos.php');
		}else{
			echo $sql;
			echo '<br><hr>Error de consulta';
		}

		?>
		

		<?php
		break;
	case 'insertar':
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		// INSERTAR ///////////////////////////////////////////
		///////////////////////////////////////////////////////
		?>
		<h2>
			Alta de producto - 
			<small>
				Insertar un nuevo producto
			</small>
			-
			<small>
				<a href="index.php">Cancelar</a>
			</small>
		</h2>
		<br>
		<form action="index.php?p=productos.php&accion=insercion" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">

				<label for="nombrePro">Nombre:</label>
				<input type="text" name="nombrePro" id="nombrePro" class="form-control">

				<label for="precioPro">Precio:</label>
				<input type="text" name="precioPro" id="precioPro" class="form-control">

				<label for="descripcionPro">descripcion:</label>
				<textarea rows="3" cols="30" name="descripcionPro" id="descripcionPro" class="form-control"></textarea>

				<label for="imagenPro">Imagen:</label>
				<input type="file" name="imagenPro" id="imagenPro" class="form-control">

				<label for="idCat">Categoria:</label>
				<select name="idCat" id="idCat" class="form-control">
					<?php  
						$sqlCat="SELECT * FROM categorias ORDER BY nombreCat ASC";
						$consultaCat=mysqli_query($conexion, $sqlCat);
						while($rCat=mysqli_fetch_array($consultaCat)){
							?>
							<option value="<?php echo $rCat['idCat']; ?>">
								<?php echo $rCat['nombreCat']; ?>
							</option>
							<?php
						}
					?>
				</select>

				<br><hr>
				<input type="submit" value="Alta producto" name="insertar" class="btn btn-default">
			</div>
		</form>	
		<?php
		break;
	case 'insercion':
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		// INSERCION //////////////////////////////////////////
		///////////////////////////////////////////////////////
		?>
		<h2>
			Alta de producto - 
			<small>
				Insercion de producto
			</small>
		</h2>
		<br>
		<?php  
		//Recojo los datos que quiero insertar
		$nombrePro=$_POST['nombrePro'];
		$precioPro=$_POST['precioPro'];
		$descripcionPro=$_POST['descripcionPro'];
		$fechaPro=date('Y-m-d H:i:s'); //Fecha actual en formato SQL
		$idCat=$_POST['idCat'];

		if(is_uploaded_file($_FILES['imagenPro']['tmp_name'])){
			$imagenPro=time().'_'.$_FILES['imagenPro']['name'];
			move_uploaded_file($_FILES['imagenPro']['tmp_name'], 'imagenes/productos/'.$imagenPro);
		}else{
			$imagenPro='ninguna.png';
		}

		//Pensamos la pregunta a SQL
		$sql="INSERT INTO productos(nombrePro, precioPro, descripcionPro, fechaPro, imagenPro, idCat)VALUES('$nombrePro', '$precioPro', '$descripcionPro', '$fechaPro', '$imagenPro', '$idCat')";

		//Realizamos la pregunta
		$consulta=mysqli_query($conexion, $sql);

		if($consulta==true){
			echo 'Consulta realizada con exito';
			header('Location:index.php?p=productos.php');
		}else{
			echo $sql;
			echo '<br><hr>Error de consulta';
		}
		?>


		<?php
		break;
	case 'modificar':
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		// MODIFICAR //////////////////////////////////////////
		///////////////////////////////////////////////////////
		?>


		<?php  
		//Recojo el id del producto que quiero modificar
		$id=$_GET['id'];

		//Pienso la pregunta
		$sql="SELECT * FROM productos WHERE idPro=$id";

		//Ejecuto la consulta
		$consulta=mysqli_query($conexion, $sql);

		//Extraigo ese unico producto
		$fila=mysqli_fetch_array($consulta);
		?>
		<h2>
			Modificar producto - 
			<small>
				Modificar un producto
			</small>
			-
			<small>
				<a href="index.php">Cancelar</a>
			</small>
		</h2>
		<br>

		<form action="index.php?p=productos.php&accion=modificacion" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">

				<label for="nombrePro">Nombre:</label>
				<input type="text" name="nombrePro" id="nombrePro" class="form-control" value="<?php echo $fila['nombrePro'];?>">

				<label for="precioPro">Precio:</label>
				<input type="text" name="precioPro" id="precioPro" class="form-control" value="<?php echo $fila['precioPro'];?>">

				<label for="descripcionPro">descripcion:</label>
				<textarea rows="3" cols="30" name="descripcionPro" id="descripcionPro" class="form-control"><?php echo $fila['descripcionPro'];?></textarea>

				<br>
				<div class="row">
					<div class="col-md-1">
						<img src="imagenes/productos/<?php echo $fila['imagenPro'];?>" width="100"><br>
					</div>
					<div class="col-md-11">
						<label for="imagenPro">Imagen:</label>
						<input type="file" name="imagenPro" id="imagenPro" class="form-control">
					</div>
				</div>
				<br>

				<input type="hidden" name="imagen_actual" value="<?php echo $fila['imagenPro'];?>">

				<input type="hidden" name="idPro" value="<?php echo $fila['idPro'];?>">

				<label for="idCat">Categoria:</label>
				<select name="idCat" id="idCat" class="form-control">
					<?php  
						$sqlCat="SELECT * FROM categorias ORDER BY nombreCat ASC";
						$consultaCat=mysqli_query($conexion, $sqlCat);
						while($rCat=mysqli_fetch_array($consultaCat)){
							if($rCat['idCat']==$fila['idCat']){
								$act='selected';
							}else{
								$act='';
							}
							?>
							<option value="<?php echo $rCat['idCat']; ?>" <?php echo $act; ?>>
								<?php echo $rCat['nombreCat']; ?>
							</option>
							<?php
						}
					?>
				</select>

				<input type="submit" value="Guardar producto" name="modificar" class="btn btn-default">
			</div>
		</form>


		<?php
		break;
	case 'modificacion':
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		// MODIFICACION ///////////////////////////////////////
		///////////////////////////////////////////////////////
		?>
		
		<h2>
			Modificar producto - 
			<small>
				Modificacion un producto
			</small>
		</h2>
		<br>
		<?php  
		//Si tengo permisos para realizar esta accion
		// if($_SESSION['conectado']){
		// 	if($_SESSION['usuario']['tipoUsu']=='administrador'){

		//Recojo los datos
		$nombrePro=$_POST['nombrePro'];
		$precioPro=$_POST['precioPro'];
		$descripcionPro=$_POST['descripcionPro'];
		$idPro=$_POST['idPro'];
		$fechaPro=date('Y-m-d H:i:s'); //Si queremos cambiar la fecha

		$idCat=$_POST['idCat'];

		$imagen_actual=$_POST['imagen_actual'];

		if(is_uploaded_file($_FILES['imagenPro']['tmp_name'])){
			$imagenPro=time().'_'.$_FILES['imagenPro']['name'];
			move_uploaded_file($_FILES['imagenPro']['tmp_name'], 'imagenes/productos/'.$imagenPro);
			
			if($imagen_actual!='ninguna.png'){
				unlink('imagenes/productos/'.$imagen_actual);
			}

		}else{
			$imagenPro=$imagen_actual;
		}

		//Preparamos la pregunta
		$sql="UPDATE productos SET nombrePro='$nombrePro', precioPro='$precioPro', descripcionPro='$descripcionPro', fechaPro='$fechaPro', imagenPro='$imagenPro', idCat='$idCat' WHERE idPro=$idPro";

		//Realizamos la pregunta
		$consulta=mysqli_query($conexion, $sql);

		if($consulta==true){
			echo 'Consulta realizada con exito';
			header('Location:index.php?p=productos.php');
		}else{
			echo $sql;
			echo '<br><hr>Error de consulta';
		}
		?>

		<?php
		break;

	case 'ver':
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		// Ver ELEMENTO ///////////////////////////////
		///////////////////////////////////////////////////////
		//Recojo el id del producto que quiero ver
		$id=$_GET['id'];

		if(isset($_POST['insertarCom'])){

			$tituloCom=$_POST['tituloCom'];
			$textoCom=$_POST['textoCom'];
			$fechaCom=Date('Y-m-d H:i:s');
			$idUsu=$_SESSION['usuario']['idUsu'];
			$idPro=$id;

			$sql="INSERT INTO comentarios(tituloCom, textoCom, fechaCom, idUsu, idPro)VALUES('$tituloCom', '$textoCom', '$fechaCom', '$idUsu', '$idPro')";

			$consulta=mysqli_query($conexion, $sql);
		}


		//Pienso la pregunta
		$sql="SELECT * FROM productos INNER JOIN categorias ON categorias.idCat=productos.idCat WHERE idPro=$id";
		//Ejecuto la consulta
		$consulta=mysqli_query($conexion, $sql);
		//Extraigo ese unico producto
		$fila=mysqli_fetch_array($consulta);
		?>
		
		<h2>
			Detalle de producto 
		</h2>
		<br>
		<article>
				<header>
					<h2>
						<?php echo $fila['nombrePro'];?>
						(<?php echo $fila['nombreCat'];?>)
						<small>
							<?php 
							if($_SESSION['conectado']){
								if($_SESSION['usuario']['tipoUsu']=='administrador'){
									?>

							<a href="index.php?p=productos.php&accion=borrar&id=<?php echo $fila['idPro'];?>" onClick="if(!confirm('Estas seguro?')){return false;};">Borrar</a>
							
							<a href="index.php?p=productos.php&accion=modificar&id=<?php echo $fila['idPro'];?>">Modificar</a>
							<?php }} ?>

							<a href="index.php?p=productos.php&accion=ver&id=<?php echo $fila['idPro'];?>&idCompra=<?php echo $fila['idPro'];?>">
							<img src="imagenes/carrito.png" width="70" alt="">
							</a>


						</small>
					</h2>
				</header>
				<section>
					<img src="imagenes/productos/<?php echo $fila['imagenPro'];?>" width="100" style="float:left;">
					<?php echo $fila['descripcionPro'];?>
				</section>
			</article>
			<div style="clear:both;"></div>
			<hr>

			<h3>
				Comentarios del producto 
			</h3>
			<hr>

			<?php  
			$sqlCom="SELECT * FROM comentarios INNER JOIN usuarios ON comentarios.idUsu=usuarios.idUsu WHERE idPro=".$fila['idPro'];
			$consultaCom=mysqli_query($conexion, $sqlCom);
			while($filaCom=mysqli_fetch_array($consultaCom)){
				?>
				<article>
					<h4><?php echo $filaCom['tituloCom']; ?> <small>(por <?php echo $filaCom['nombreUsu']; ?>)</small></h4>
					<section><?php echo $filaCom['textoCom']; ?></section>
					<footer><?php echo $filaCom['fechaCom']; ?></footer>
				</article>
				<?php
			}
			?>
			<hr>
			<?php if($_SESSION['conectado']==true){ ?>
			<!-- form>input:text+textarea+input:submit -->
			<form action="index.php?p=productos.php&accion=ver&id=<?php echo $id;?>" method="post">
				<input type="text" name="tituloCom">
				<textarea name="textoCom" cols="30" rows="10"></textarea>
				<input type="submit" value="Insertar" name="insertarCom">
			</form>
			<?php } ?>
		<?php

		break;

	case 'listado':
	default:
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		// LISTADO DE ELEMENTOS ///////////////////////////////
		///////////////////////////////////////////////////////
		?>
		<h2>
			Listado de productos - 
			<small>
				<?php 
				if($_SESSION['conectado']){
					if($_SESSION['usuario']['tipoUsu']=='administrador'){
						?>
						<a href="index.php?p=productos.php&accion=insertar">
							Dar de alta un nuevo producto
						</a>
						<?php
					}
				}
				?>
			</small>
		</h2>
		<br>

		<?php  
		//Pensar la pregunta que quiero hacer
		$sql="SELECT * FROM productos INNER JOIN categorias ON productos.idCat=categorias.idCat ORDER BY fechaPro DESC";

		//Realizamos la pregunta
		$consulta=mysqli_query($conexion, $sql);

		//Analizamos la respuesta
		while($fila=mysqli_fetch_array($consulta)){
			?>
			<article>
				<header>
					<h2>
						<a name="producto<?php echo $fila['idPro'];?>"></a>
						
						<!-- <a href="index.php?p=productos.php&accion=ver&id=<?php echo $fila['idPro'];?>"> -->

						<?php 
						$nombre=str_replace(' ', '-', $fila['nombrePro']);
						$nombre=strtolower($nombre);
						?>

						<a href="cerveza-<?php echo $nombre;?>-<?php echo $fila['idPro'];?>.html">

							<?php echo $fila['nombrePro'];?>

						</a>

						(<?php echo $fila['nombreCat'];?>)
						-
						<small>
						
							<?php 
							if($_SESSION['conectado']){
								if($_SESSION['usuario']['tipoUsu']=='administrador'){
									?>
									<a href="index.php?p=productos.php&accion=borrar&id=<?php echo $fila['idPro'];?>" onClick="if(!confirm('Estas seguro?')){return false;};">Borrar</a>
									
									<a href="index.php?p=productos.php&accion=modificar&id=<?php echo $fila['idPro'];?>">Modificar</a>
							<?php }} ?>

						<a href="index.php?p=productos.php&idCompra=<?php echo $fila['idPro'];?>#producto<?php echo $fila['idPro'];?>">
							<img src="imagenes/carrito.png" width="70" alt="">
						</a>

						</small>
					</h2>
				</header>
				<section>
					<img src="imagenes/productos/<?php echo $fila['imagenPro'];?>" width="100" style="float:left;">
					<?php echo $fila['descripcionPro'];?>
				</section>
			</article>
			<div style="clear:both;"></div>
			<hr>
			<?php
		}
		?>



		<?php
		break;
}

?>