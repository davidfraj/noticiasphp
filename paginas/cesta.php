<?php  
//Realizo las acciones de guardar
if(isset($_POST['guardar'])){
	//Con esto actualizo las unidades si fuera necesario
	$uniDesplegableCarrito=$_POST['uniDesplegableCarrito'];
	foreach ($_SESSION['carrito'] as $key => $value) {
		$_SESSION['uniCarrito'][$key]=$uniDesplegableCarrito[$key];
	}

	//Elimino las lineas de productos si fuera necesario
	if(isset($_POST['paraEliminar'])){
		$cont=0;
		$paraEliminar=$_POST['paraEliminar'];
		foreach ($paraEliminar as $key => $value) {
			//Eliminamos el producto del carrito
			array_splice($_SESSION['uniCarrito'], ($value-$cont), 1);
			array_splice($_SESSION['carrito'], ($value-$cont), 1);
			$cont++;
		}
		//var_dump($paraEliminar); //Con esto puedo mostrar TODO el array
	}
}
?>


<h2>
	Cesta de la compra
</h2>
<hr>
<form action="index.php?p=cesta.php" method="post">
<table class="table">
	<tr>
		<th>Nombre</th>
		<th>Precio</th>
		<th>Unidades</th>
		<th>Total</th>
		<th>Eliminar</th>
	</tr>

	<?php  
	$total=0;
	foreach ($_SESSION['carrito'] as $key => $value) {
		$sql="SELECT * FROM productos WHERE idPro=$value";
		$consulta=mysqli_query($conexion, $sql);
		$fila=mysqli_fetch_array($consulta);

		$subtotal=$_SESSION['uniCarrito'][$key]*$fila['precioPro'];
		$total+=$subtotal;
		?>
		<tr>
			<td><?php echo $fila['nombrePro']; ?></td>
			<td><?php echo $fila['precioPro']; ?></td>
			<td>
				<select name="uniDesplegableCarrito[]">
				<?php  
				for($i=1;$i<=20;$i++){
					if($i==$_SESSION['uniCarrito'][$key]){
						$s='selected';
					}else{
						$s='';
					}
					?>
					<option value="<?php echo $i;?>" <?php echo $s;?>>
						<?php echo $i;?>
					</option>
					<?php
				}
				?>
				</select>
			</td>
			<td><?php echo $subtotal;?></td>
			<td><input type="checkbox" name="paraEliminar[]" value="<?php echo $key; ?>"></td>
		</tr>
		<?php  
	}
	?>
</table>
<input type="submit" name="guardar" value="Guardar cambios">
</form>
<hr>
Total: <?php echo $total; ?> €<br>
<?php echo count($_SESSION['carrito']) ?> productos en cesta
<hr>
<!-- <a href="index.php?p=finalizar.php">Pedir la dolorosa - que paga Fabrizio</a> -->
<a href="finalizar.html">Pedir la dolorosa - que paga Fabrizio</a>
<br>
<br>
<br>
<br>
<br>