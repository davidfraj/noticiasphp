-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-05-2017 a las 16:02:40
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `noticiasphp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCat` int(11) NOT NULL,
  `nombreCat` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCat`, `nombreCat`) VALUES
(1, 'Cerveza Negra'),
(2, 'Cerveza Tostada'),
(3, 'Cerveza Rubia'),
(4, 'Cerveza para Celíacos'),
(5, 'Cerveza casera'),
(6, 'Cerveza Hiper Tostada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `idCom` int(11) NOT NULL,
  `tituloCom` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `textoCom` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fechaCom` datetime NOT NULL,
  `idUsu` int(11) NOT NULL,
  `idPro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`idCom`, `tituloCom`, `textoCom`, `fechaCom`, `idUsu`, `idPro`) VALUES
(1, 'titulo del comentario', 'Texto completo de mi comentario', '2017-05-23 17:08:28', 5, 9),
(2, 'otro titulo', 'otro texto que sea', '2017-05-23 17:09:33', 5, 9),
(3, 'titulo del comentario por david', 'Aqui pongo el texto sobre el comentario de lo que sea en la web de la cerveza PULANER', '2017-05-23 17:10:12', 1, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `idPed` int(11) NOT NULL,
  `fechaPed` datetime NOT NULL,
  `numeroPed` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idUsu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idPro` int(11) NOT NULL,
  `nombrePro` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `precioPro` double NOT NULL,
  `descripcionPro` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fechaPro` datetime NOT NULL,
  `imagenPro` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `idCat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idPro`, `nombrePro`, `precioPro`, `descripcionPro`, `fechaPro`, `imagenPro`, `idCat`) VALUES
(9, 'Paulaner', 2.6, 'Paulaner Brauerei GmbH & Co KG es una empresa cervecera alemana, fundada en 1634 en Múnich por los frailes Mínimos del claustro de Neudeck ob der Au. El orden mendicante y la cervecería llevan el nombre de Francisco de Paula, el fundador de la orden. Paulaner es una de las seis fábricas de cerveza que ofrecen cerveza para la Oktoberfest, la fiesta de la cerveza alemana que data de 1810', '2017-05-17 15:43:10', '1495028590_hefewbnaturtrub_05l_flasche.png', 2),
(10, 'Guiness', 3, 'Guinness (pronunciación en AFI : /ˡgɪ.nəs/) es una cerveza negra seca del tipo stout elaborada por primera vez por el cervecero Arthur Guinness en la empresa cervecera denominada St. Jamess Gate Brewery ubicada en la ciudad de Dublín, Irlanda. Guinness se elabora desde el año 1759. La cerveza se basa en el estilo porter, originado en Londres a principios de 1700. Se trata de una de las marcas de cerveza más conocidas y se exporta a la mayor parte de los países. Ha llegado a batir a muchos imitadores. La característica distintiva en el sabor es la cebada tostada que se mantiene sin fermentar. Durante muchos años una parte de la cerveza se envejecía para dar un sabor láctico, pero Guinness ha renunciado confirmar si esto sigue ocurriendo. La gruesa y cremosa espuma es resultado de una mezcla de nitrógeno añadida en el envasado. Lo que está comprobado es que no lleva café, pese a la creencia popular.', '2017-05-17 15:42:32', '1495027509_150px-Guinness.jpg', 1),
(11, 'Estrella Galicia', 1.2, 'Después de su regreso de México a finales del siglo XIX, D. Jose Mª Rivera Corral funda en 1906 la fábrica La Estrella de Galicia en la ciudad de La Coruña, dedicada a la fabricación de cervezas y hielo, nombre que rememora lo que fue su negocio en Veracruz “La Estrella de Oro”, y que apostaba por un producto, en aquellos tiempos, de consumos muy reducidos.', '2017-05-12 19:34:14', '1494610454_estrella_galicia.jpg', 3),
(13, 'aaa', 12, 'asd', '2017-05-16 16:37:17', '1494945437_20090518111206.jpg', 0),
(14, 'aaa', 12, 'asd', '2017-05-16 16:37:35', '1494945455_minecraft 5.jpg', 0),
(15, 'Heineken', 1.2, 'Cerveza rubia, de consumo habitual', '2017-05-17 15:23:42', '1495027422_Buy-Heineken-Beer-Heineken-Imported-Beer-Heineken.jpg', 3),
(16, 'Franziskaner', 2.5, 'Cerveza FRANZISKANER', '2017-05-17 15:25:01', '1495027501_27831-FRANZISKANER.jpg', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productosenpedido`
--

CREATE TABLE `productosenpedido` (
  `idProPed` int(11) NOT NULL,
  `idPro` int(11) NOT NULL,
  `idPed` int(11) NOT NULL,
  `precioProPed` float NOT NULL,
  `unidadesProPed` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsu` int(11) NOT NULL,
  `nombreUsu` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `correoUsu` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `claveUsu` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `fechaAltaUsu` datetime NOT NULL,
  `tipoUsu` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `sessionUsu` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsu`, `nombreUsu`, `correoUsu`, `claveUsu`, `fechaAltaUsu`, `tipoUsu`, `sessionUsu`) VALUES
(1, 'David Fraj Blesa', 'davidfraj@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '2017-05-18 16:00:00', 'administrador', ''),
(2, 'Fulanito de tal', 'fulanito@de.com', '81dc9bdb52d04dc20036dbd8313ed055', '2017-05-18 17:00:00', 'estandard', ''),
(3, 'Gerome de la sierra', 'gerome@gmail.com', '674f3c2c1a8a6f90461e8a66fb5550ba', '2017-05-18 17:35:00', 'estandard', ''),
(4, 'Marco Polo', 'marco@polo.com', 'd41d8cd98f00b204e9800998ecf8427e', '2017-05-19 16:34:09', 'estandard', ''),
(5, 'Paco leon', 'paco@leon.com', '25f9e794323b453885f5181f1b624d0b', '2017-05-19 16:47:13', 'estandard', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCat`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idCom`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`idPed`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idPro`);

--
-- Indices de la tabla `productosenpedido`
--
ALTER TABLE `productosenpedido`
  ADD PRIMARY KEY (`idProPed`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `idCom` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `idPed` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idPro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `productosenpedido`
--
ALTER TABLE `productosenpedido`
  MODIFY `idProPed` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
